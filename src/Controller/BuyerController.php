<?php

namespace App\Controller;

use App\Service\Api;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/buyer")
 */
class BuyerController extends AbstractController
{
    private $api;

    public function __construct(Api $api)
    {
        $this->api = $api;
    }

    /**
     * @Route("/signup", name="signup_buyer_first")
     */
    public function signupFirst()
    {
        // enable cross origin : autoriser request from localhost mobile
        header("Access-Control-Allow-Origin: * ");

        // récup params fields
        $request = Request::createFromGlobals();
        $result = $this->api->getFromApi('buyer/signup', [
            "firstName" =>  $request->get('firstName'),
            "lastName" =>  $request->get('lastName'),
            "email" =>  $request->get('email'),
            "phone" =>  $request->get('phone'),
            "password" =>  $request->get('password'),
            "passwordConfirmation" =>  $request->get('passwordConfirmation')

        ], true);

        if ($result['status'] != 200) {
            return new JsonResponse(['status' => 500, 'message' => 'insertion error'], 500);
        }

        return new JsonResponse(['status' => 200, 'result' => $result['result']], 200);
    }


    /**
     * @Route("/confirm/phone/request", name="confirmPhoneRequestBuyer")
     */
    public function confirmPhoneRequest()
    {
        header("Access-Control-Allow-Origin: * ");

        $request = Request::createFromGlobals();

        $result = $this->api->getFromApi('buyer/phone/confirm/request', [
            "buyerId" =>  $request->get('idBuyer'),

        ], true);

        if ($result['status'] != 200) {
            return new JsonResponse(['status' => 500, 'message' => 'error API'], 500);
        }

        return new JsonResponse(['status' => 200, 'result' => $result], 200);
    }

    /**
     * @Route("/confirm/phone/validation", name="confirmPhoneValidationBuyer")
     */
    public function confirmPhoneValidation()
    {
        header("Access-Control-Allow-Origin: * ");

        $request = Request::createFromGlobals();

        $result = $this->api->getFromApi('buyer/phone/validation', [
            "buyerId" =>  $request->get('idBuyer'),
            "code" =>  $request->get('code'),

        ], true);

        if ($result['status'] != 200) {
            return new JsonResponse(['status' => 500, 'message' => 'error API'], 500);
        }

        return new JsonResponse(['status' => 200, 'result' => $result], 200);
    }


    /**
     * @Route("/confirm/email/request", name="confirmEmailRequestBuyer")
     */
    public function confirmEmailRequest()
    {
        header("Access-Control-Allow-Origin: * ");

        $request = Request::createFromGlobals();

        $result = $this->api->getFromApi('buyer/email/confirm/request', [
            "buyerId" =>  $request->get('idBuyer'),

        ], true);

        if ($result['status'] != 200) {
            return new JsonResponse(['status' => 500, 'message' => 'error API'], 500);
        }

        return new JsonResponse(['status' => 200, 'result' => $result], 200);
    }

    /**
     * @Route("/confirm/email/validation", name="confirmEmailValidationBuyer")
     */
    public function confirmEmailValidation()
    {
        header("Access-Control-Allow-Origin: * ");

        $request = Request::createFromGlobals();

        $result = $this->api->getFromApi('buyer/email/validation', [
            "buyerId" =>  $request->get('idBuyer'),
            "code" =>  $request->get('code'),

        ], true);

        if ($result['status'] != 200) {
            return new JsonResponse(['status' => 500, 'message' => 'error API'], 500);
        }

        return new JsonResponse(['status' => 200, 'result' => $result], 200);
    }

    /**
     * @Route("/login", name="login_buyer")
     */
    public function login()
    {
        // enable cross origin : autoriser request from localhost mobile
        header("Access-Control-Allow-Origin: * ");

        // récup params fields
        $request = Request::createFromGlobals();
        $result = $this->api->getFromApi('buyer/login', [
            "email" =>  $request->get('email'),
            "password" =>  $request->get('password'),

        ], true);

        if ($result['status'] != 200) {
            return new JsonResponse(['status' => 500, 'message' => 'insertion error'], 500);
        }

        return new JsonResponse(['status' => 200, 'result' => $result['result']], 200);
    }
}
