<?php

namespace App\Controller;

use App\Service\Api;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/typeOperation")
 */
class TypeOperationController extends AbstractController
{
  private $api;

  public function __construct(Api $api)
  {
    $this->api = $api;
  }

  /**
   * @Route("/list", name="type_operation_list")
   */
  public function typeOperationList()
  {
    // enable cross origin : autoriser request from localhost mobile
    header("Access-Control-Allow-Origin: * ");

    // récup params fields
    $request = Request::createFromGlobals();

    $result = $this->api->getFromApi('type_operations');

    if ($result['status'] != 200) {
      return new JsonResponse(['status' => 500, 'message' => 'insertion error'], 500);
    }

    return new JsonResponse(['status' => 200, 'result' => $result['result']], 200);
  }
}
