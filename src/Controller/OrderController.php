<?php

namespace App\Controller;

use App\Service\Api;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/order")
 */
class OrderController extends AbstractController
{
    private $api;

    public function __construct(Api $api)
    {
        $this->api = $api;
    }


    /**
     * @Route("/add", name="order_add")
     */
    public function add()
    {
        header("Access-Control-Allow-Origin: * ");
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

        $request_body = file_get_contents('php://input');
        $data = json_decode($request_body, 1);

        $card = json_encode($data);

        $result = $this->api->getFromApi('orders/add', [
            "card" =>  $card,

        ], true, 1);

        if (!isset($result['id'])) {
            return new JsonResponse(['status' => 500, 'message' => 'insertion error'], 500);
        }

        return new JsonResponse(['status' => 200, 'result' => $result], 200);
    }

    /**
     * @Route("/detail/farm", name="order_detail_farm")
     */
    public function orderDetailFarm()
    {
        header("Access-Control-Allow-Origin: * ");

        $request = Request::createFromGlobals();


        $result = $this->api->getFromApi('cards', [
            "farm" =>  $request->get('farmId'),
            "ordre.paid" =>  true,
        ]);

        $ordersLinks = [];
        foreach ($result as $k => $r) {
            if (in_array($r['ordre'], $ordersLinks)) continue;
            $ordersLinks[] = $r['ordre'];

            $order = $this->api->getFromApi(str_replace($_ENV['API_RELATION_URL'], '', $r['ordre']));

            if (!array_key_exists('buyer', $order)) {
                continue;
            }

            $buyer = $this->api->getFromApi(str_replace($_ENV['API_RELATION_URL'], '', $order['buyer']));
            $result[$k]['buyer'] = $buyer;
            $result[$k]['order'] = $order;
        }

        return new JsonResponse(['status' => 200, 'result' => $result], 200);
    }

    /**
     * @Route("/detail", name="order_detail")
     */
    public function orderDetail()
    {
        header("Access-Control-Allow-Origin: * ");

        $request = Request::createFromGlobals();


        $result = $this->api->getFromApi('orders/' . $request->get('orderId'));


        $buyer = $this->api->getFromApi(str_replace($_ENV['API_RELATION_URL'], '', $result['buyer']));
        $result['buyer'] = $buyer;

        $cards = $this->api->getFromApi('cards', ['ordre' => $result['id']]);
        $products = [];
        foreach ($cards as $k => $c) {
            $cards[$k]['product'] = $this->api->getFromApi(str_replace($_ENV['API_RELATION_URL'], '', $c['product']));
        }
        $result['cards'] = $cards;

        return new JsonResponse(['status' => 200, 'result' => $result], 200);
    }


    /**
     * @Route("/pay", name="order_pay")
     */
    public function pay()
    {
        header("Access-Control-Allow-Origin: * ");
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

        $request = Request::createFromGlobals();

        $result = $this->api->put('orders/' . $request->get('orderId'), [
            "paid" =>  true,
            "paidAt" => date('Y-m-d h:i:s')
        ]);

        if (!isset($result['id'])) {
            return new JsonResponse(['status' => 500, 'message' => 'insertion error'], 500);
        }

        return new JsonResponse(['status' => 200, 'result' => $result], 200);
    }
}
