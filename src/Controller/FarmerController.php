<?php

namespace App\Controller;

use App\Service\Api;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/farmer")
 */
class FarmerController extends AbstractController
{
    private $api;

    public function __construct(Api $api)
    {
        $this->api = $api;
    }

    /**
     * @Route("/signup/first", name="signup_farmer_first")
     */
    public function signupFirst()
    {
        // enable cross origin : autoriser request from localhost mobile
        header("Access-Control-Allow-Origin: * ");

        // récup params fields
        $request = Request::createFromGlobals();

        $result = $this->api->getFromApi('farm/signup/first', [
            "firstName" =>  $request->get('firstName'),
            "lastName" =>  $request->get('lastName'),
            "email" =>  $request->get('email'),
            "phone" =>  $request->get('phone'),
            "password" =>  $request->get('password'),
            "passwordConfirmation" =>  $request->get('passwordConfirmation')

        ], true);

        if ($result['status'] != 200) {
            return new JsonResponse(['status' => 500, 'message' => 'insertion error'], 500);
        }

        return new JsonResponse(['status' => 200, 'result' => $result['result']], 200);
    }

    /**
     * @Route("/signup/second", name="signup_farmer_second")
     */
    public function signupSecond()
    {
        header("Access-Control-Allow-Origin: * ");

        $request = Request::createFromGlobals();

        $result = $this->api->getFromApi('farm/signup/second', [
            "idFarm" =>  $request->get('idFarm'),
            "name" =>  $request->get('name'),
            //"address" =>  $request->get('address'),
            //"city" =>  $request->get('city'),
            //"postalCode" =>  $request->get('postalCode'),
            //"country" =>  $request->get('country'),
            "typeOperationId" =>  $request->get('typeOperationId')

        ], true);

        if ($result['status'] != 200) {
            return new JsonResponse(['status' => 500, 'message' => 'insertion error'], 500);
        }

        return new JsonResponse(['status' => 200, 'result' => $result['result']], 200);
    }


    /**
     * @Route("/confirm/phone/request", name="confirmPhoneRequest")
     */
    public function confirmPhoneRequest()
    {
        header("Access-Control-Allow-Origin: * ");

        $request = Request::createFromGlobals();

        $result = $this->api->getFromApi('farm/phone/confirm/request', [
            "farmId" =>  $request->get('idFarm'),

        ], true);

        if ($result['status'] != 200) {
            return new JsonResponse(['status' => 500, 'message' => 'error API'], 500);
        }

        return new JsonResponse(['status' => 200, 'result' => $result], 200);
    }

    /**
     * @Route("/confirm/phone/validation", name="confirmPhoneValidation")
     */
    public function confirmPhoneValidation()
    {
        header("Access-Control-Allow-Origin: * ");

        $request = Request::createFromGlobals();

        $result = $this->api->getFromApi('farm/phone/validation', [
            "farmId" =>  $request->get('idFarm'),
            "code" =>  $request->get('code'),

        ], true);

        if ($result['status'] != 200) {
            return new JsonResponse(['status' => 500, 'message' => 'error API'], 500);
        }

        return new JsonResponse(['status' => 200, 'result' => $result], 200);
    }


    /**
     * @Route("/confirm/email/request", name="confirmEmailRequest")
     */
    public function confirmEmailRequest()
    {
        header("Access-Control-Allow-Origin: * ");

        $request = Request::createFromGlobals();

        $result = $this->api->getFromApi('farm/email/confirm/request', [
            "farmId" =>  $request->get('idFarm'),

        ], true);

        if ($result['status'] != 200) {
            return new JsonResponse(['status' => 500, 'message' => 'error API'], 500);
        }

        return new JsonResponse(['status' => 200, 'result' => $result], 200);
    }

    /**
     * @Route("/confirm/email/validation", name="confirmEmailValidation")
     */
    public function confirmEmailValidation()
    {
        header("Access-Control-Allow-Origin: * ");

        $request = Request::createFromGlobals();

        $result = $this->api->getFromApi('farm/email/validation', [
            "farmId" =>  $request->get('idFarm'),
            "code" =>  $request->get('code'),

        ], true);

        if ($result['status'] != 200) {
            return new JsonResponse(['status' => 500, 'message' => 'error API'], 500);
        }

        return new JsonResponse(['status' => 200, 'result' => $result], 200);
    }

    /**
     * @Route("/set/memberShipType", name="setMemberShipType")
     */
    public function setMemberShipType()
    {
        header("Access-Control-Allow-Origin: * ");

        $request = Request::createFromGlobals();

        $result = $this->api->put('farms/' . $request->get('idFarm'), [
            'memberShipType' => $_ENV['API_RELATION_URL'] . "member_ship_types/" . $request->get('memberShipId')
        ]);

        if (!isset($result['id'])) {
            return new JsonResponse(['status' => 500, 'message' => 'error API'], 500);
        }

        return new JsonResponse(['status' => 200, 'result' => $result], 200);
    }


    /**
     * @Route("/login", name="login_farm")
     */
    public function login()
    {
        // enable cross origin : autoriser request from localhost mobile
        header("Access-Control-Allow-Origin: * ");

        // récup params fields
        $request = Request::createFromGlobals();
        $result = $this->api->getFromApi('farm/login', [
            "email" =>  $request->get('email'),
            "password" =>  $request->get('password'),

        ], true);

        if ($result['status'] != 200) {
            return new JsonResponse(['status' => 500, 'message' => 'insertion error'], 500);
        }

        return new JsonResponse(['status' => 200, 'result' => $result['result']], 200);
    }
}
