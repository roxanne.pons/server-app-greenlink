<?php

namespace App\Controller;

use App\Service\Api;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/product")
 */
class ProductController extends AbstractController
{
  private $api;

  public function __construct(Api $api)
  {
    $this->api = $api;
  }


  /**
   * @Route("/add", name="product_add")
   */
  public function add()
  {
    header("Access-Control-Allow-Origin: * ");
    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
    header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

    $request_body = file_get_contents('php://input');
    $data = json_decode($request_body, 1);

    $params = [
      //relation facilitée grace a l'api plateforme
      "farm" =>  $_ENV['API_RELATION_URL'] . 'farms/' . $data['farm'],
      "name" =>  $data['name'],
      "unity" =>  $data['unity'],
      "price" =>  $data['price'],
      "stock" =>  $data['stock'],
      "description" =>  $data['description'],
      "reduction" =>  intval($data['reduction']),
      "photos" =>  $data['photos'],
      "categoryProduct" =>  $_ENV['API_RELATION_URL'] . 'category_products/' . $data['category']

    ];

    $result = $this->api->getFromApi('products', $params, true);

    return new JsonResponse(['status' => 200, 'result' => $result], 200);
  }


  /**
   * @Route("/update", name="product_update")
   */
  public function update()
  {
    header("Access-Control-Allow-Origin: * ");

    $request = Request::createFromGlobals();

    $result = $this->api->put('products/' . $request->get('id'), [
      //relation facilitée grace a l'api plateforme
      "name" =>  $request->get('name'),
      "unity" =>  $request->get('unity'),
      "price" =>  $request->get('price'),
      "stock" =>  $request->get('stock'),
      "description" =>  $request->get('description'),
      "reduction" =>  intval($request->get('reduction')),

    ]);


    if (!isset($result['id'])) {
      return new JsonResponse(['status' => 500, 'message' => 'insertion error'], 500);
    }

    return new JsonResponse(['status' => 200, 'result' => $result], 200);
  }

  /**
   * @Route("/categories/get", name="categories_get")
   */
  public function getCategories()
  {
    header("Access-Control-Allow-Origin: * ");

    $request = Request::createFromGlobals();
    $result = $this->api->getFromApi('categories/get', [
      "categoryId" =>  $request->get('categoryId'),
      "categoryParents" => $request->get('categoryParents') == "true"
    ], 1);

    if ($result['status'] != 200) {
      return new JsonResponse(['status' => 500, 'message' => 'insertion error'], 500);
    }

    return new JsonResponse($result, 200);
  }

  /**
   * @Route("/categorie", name="categorie_get")
   */
  public function getCategorie()
  {
    header("Access-Control-Allow-Origin: * ");

    $request = Request::createFromGlobals();
    $result = $this->api->getFromApi('category_products/' . $request->get('categoryId'));

    if (!$result) {
      return new JsonResponse(['status' => 500, 'message' => 'insertion error'], 500);
    }

    return new JsonResponse($result, 200);
  }

  /**
   * @Route("/categorie/family", name="categorie_get_family")
   */
  public function getCategorieFamily()
  {
    header("Access-Control-Allow-Origin: * ");

    $request = Request::createFromGlobals();
    //appel a l'api -> recup categoryId du field category_products
    $result = $this->api->getFromApi('category_products/' . $request->get('categoryId'));

    $cats = [$result['name']];
    //temps qu'on trouve un parent, on continue de boucler
    while (isset($result['category']) && $result['category']) {
      $result = $this->api->getFromApi(str_replace($_ENV['API_RELATION_URL'], '', $result['category']));
      $cats[] = $result['name'];
    }
    //reverse pour commencer avec categorie mère
    $result = array_reverse($cats);

    if (!$result) {
      return new JsonResponse(['status' => 500, 'message' => 'insertion error'], 500);
    }

    return new JsonResponse($result, 200);
  }

  /**
   * @Route("/by/farm", name="product_by_farm")
   */
  public function productByFarm()
  {
    header("Access-Control-Allow-Origin: * ");
    //recup les produits d'une ferme 
    $request = Request::createFromGlobals();
    $result = $this->api->getFromApi('products', [
      'farm' => $request->get('farmId')
    ]);

    if (!$result) {
      return new JsonResponse(['status' => 500, 'message' => 'insertion error'], 500);
    }

    return new JsonResponse($result, 200);
  }

  /**
   * @Route("/get", name="product_get_by_id")
   */
  public function productById()
  {
    header("Access-Control-Allow-Origin: * ");

    $request = Request::createFromGlobals();
    $result = $this->api->getFromApi('products/' . $request->get('id'));

    if (!$result) {
      return new JsonResponse(['status' => 500, 'message' => 'insertion error'], 500);
    }

    return new JsonResponse($result, 200);
  }
}
