<?php

namespace App\Service;


use Symfony\Component\HttpFoundation\Session\SessionInterface;
use function Symfony\Component\DependencyInjection\Loader\Configurator\param;

class Api
{

  private $session;

  public function __construct(SessionInterface $session)
  {
    $this->session = $session;
  }

  public function auth($parameters)
  {

    if (!isset($parameters['isPost'])) $parameters['isPost'] = true;
    if (!isset($parameters['url']) || !isset($parameters['fields'])) return false;

    $headers = array();
    $headers[] = 'Content-Type: application/json';
    $params = array(
      CURLOPT_URL => $parameters['url'],
      CURLOPT_POST => $parameters['isPost'],
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_HEADER => false,
      CURLOPT_HTTPHEADER => $headers,
      CURLOPT_POSTFIELDS => json_encode($parameters['fields']),
      CURLOPT_SSL_VERIFYPEER => false
    );

    $ch = curl_init();
    curl_setopt_array($ch, $params);
    $result = curl_exec($ch);

    if (curl_error($ch)) {
      //echo 'Error:' . curl_error($ch);
      return 'Error:' . curl_error($ch);
    }
    curl_close($ch);
    return json_decode($result, 1);
  }

  public function request($parameters, $die = false)
  {
    if (!isset($parameters['isPost'])) $parameters['isPost'] = true;
    if (!isset($parameters['isPut'])) $parameters['isPut'] = false;
    if (!isset($parameters['url'])) return false;
    if (!isset($parameters['fields'])) $parameters['fields'] = [];

    // token résultat de la requete auth
    $headers = array();
    if (isset($parameters['token'])) {
      $headers[] = "Authorization: Bearer " . $parameters['token'];
    }


    $headers[] = 'Content-Type: application/json';
    $headers[] = 'Accept: application/json';
    $params = array(
      CURLOPT_URL => $parameters['url'],
      CURLOPT_POST => $parameters['isPost'],
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_HEADER => false,
      CURLOPT_HTTPHEADER => $headers,

      CURLOPT_SSL_VERIFYPEER => false
    );

    if ($parameters['isPut'] == true) {
      $params[CURLOPT_CUSTOMREQUEST] = "PUT";
    }

    if ($parameters['isPost'] == true || $parameters['isPut'] == true) {
      if (isset($parameters['fields'])) {

        $params[CURLOPT_POSTFIELDS] = json_encode($parameters['fields']);
      }
    } else {
      if (isset($parameters['fields']) && $parameters['fields']) {
        $params[CURLOPT_URL] .= '?' . http_build_query($parameters['fields']);
      }
    }

    $ch = curl_init();
    curl_setopt_array($ch, $params);
    $result = curl_exec($ch);

    // évite de faire des dmps coté api
    if ($die) {
      echo $result;
      die;
    }

    if (curl_error($ch)) {
      //echo 'Error:' . curl_error($ch);
      return 'Error:' . curl_error($ch);
    }
    curl_close($ch);

    return json_decode($result, 1);
  }

  public function getFromApi($url, $params = [], $isPost = false,  $die = false)
  {

    $result = $this->request([
      'url' => $_ENV['API_URL'] . $url,
      'token' => $this->session->get('_token_muta_api'),
      'isPost' => $isPost,
      'fields' => $params
    ], $die);


    if (isset($result['code']) && $result['code'] == 401) {
      $output = $this->auth([
        'url' => $_ENV['API_URL'] . 'login_check',
        'fields' => [
          'username' => $_ENV['API_USER'],
          'password' => $_ENV['API_PASSWORD']
        ],
      ]);

      if (!isset($output['token'])) {
        // @todo
      }

      $this->session->set('_token_muta_api', $output['token']);
      $result = $this->request([
        'url' => $_ENV['API_URL'] . $url,
        'token' => $this->session->get('_token_muta_api'),
        'isPost' => $isPost,
        'fields' => $params
      ], $die);
    }
    return $result;
  }


  public function put($url, $params = [], $die = false)
  {
    $output = $this->auth([
      'url' => $_ENV['API_URL'] . 'login_check',
      'fields' => [
        'username' => $_ENV['API_USER'],
        'password' => $_ENV['API_PASSWORD']
      ],
    ]);

    if (!isset($output['token'])) {
      // @todo
    }

    return $this->request([
      'url' => $_ENV['API_URL'] . $url,
      'token' => $output['token'],
      'isPost' => false,
      'isPut' => true,
      'fields' => $params
    ], $die);
  }
}
